package wiki.xsx.core.log;

/**
 * 读写类型
 * @author xsx
 * @date 2021/10/18
 * @since 1.8
 */
public enum  ReadWriteType {
    /**
     * 读
     */
    READ,
    /**
     * 写
     */
    WRITE;
}
