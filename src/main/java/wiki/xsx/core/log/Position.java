package wiki.xsx.core.log;

/**
 * 代码定位
 * @author xsx
 * @date 2019/6/21
 * @since 1.8
 */
public enum Position {
    /**
     * 开启
     */
    ENABLED,
    /**
     * 关闭
     */
    DISABLED,
    /**
     * 未知
     */
    UNKNOWN;
}
